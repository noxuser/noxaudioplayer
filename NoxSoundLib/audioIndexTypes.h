
#ifndef AUDIO_INDEX_TYPES_H__
#define AUDIO_INDEX_TYPES_H__

#include <iostream>

using AudioMeta = struct
{
	uint32_t header_t;
	uint32_t unknown_t;
	uint32_t defCount_t;
};

using AudioInfo = struct
{
	char audioName_t[0x10];
	uint32_t bagOffset_t;
	uint32_t streamLength;
	uint32_t sampleRate;
	uint32_t field2;
	uint16_t sample_align;
	uint16_t zero_field;
};

namespace AudioIdxParam
{
	constexpr uint32_t metaDataSize = sizeof(AudioMeta);
	constexpr uint32_t fieldDataSize = sizeof(AudioInfo);
}

#endif