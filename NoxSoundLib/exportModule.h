
#ifndef EXPORT_MODULE_H__
#define EXPORT_MODULE_H__

#ifdef MAKEDLL
#define EXPORT_OBJECT __declspec(dllexport)
#else
#define EXPORT_OBJECT __declspec(dllimport)
#endif

#include <memory>
#include <string>
#include <list>

//@DLL interface
class EXPORT_OBJECT ExternalLib
{
public:
	explicit ExternalLib();
	virtual ~ExternalLib();

	static std::unique_ptr<ExternalLib> MakeInstance();

	virtual bool CheckState() const = 0;
	virtual bool Initialize() = 0;
	virtual bool SendPath(const std::string &path) = 0;
	virtual void PlayWithSubkey(const std::string &subkey) = 0;
	virtual void PlayFirst(const std::string &key) = 0;
	virtual bool ExtractStream(const std::string &subkey, const std::string &output = {}) = 0;
	virtual std::list<std::string> GetAudioKeyList() = 0;
	virtual std::list<std::string> GetAudioRealnameList(const std::string &key) = 0;
};


#endif

