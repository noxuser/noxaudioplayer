
#ifndef WAV_CONTEXT_H__
#define WAV_CONTEXT_H__

#include "audioBag.h"
#include "audioIndexTypes.h"
#include <iostream>
#include <memory>

using FactSection = struct
{
	uint16_t alwaysTwo;
	uint16_t unknown1;
	uint32_t factSymb;
	uint32_t factLength;	//always 4
	uint32_t factData;
};

using WavFormatHeader = struct
{
	uint32_t header;
	uint32_t waveSize;
	uint32_t waveSymb;
	uint32_t fmtSymb;
	uint32_t fmtChunkSize;
	uint16_t audioFormat;
	uint16_t channels;
	uint32_t sampleRate;
	uint32_t byteRate;
	uint16_t sampleAlign;
	uint16_t sampleDepth;

	FactSection factField;	//!Fixme! 추후에는 필요없을 것 같지만, 아직은 필요함!//

	uint32_t dataSymb;
	uint32_t dataSize;
};

class WavContext : public AudioBag
{
private:
	std::unique_ptr<WavFormatHeader> m_wavHeaderCtx;
	std::vector<uint8_t> m_ctxStream;

public:
	explicit WavContext();
private:
	void InitFactField();
	void SetWavHeader();
	virtual std::vector<uint8_t> GetPrestream() override;
	virtual std::string OutputFileName() override;
};

#endif