
#define MAKEDLL
#include "exportModule.h"
#include "soundPlugin.h"

ExternalLib::ExternalLib()
{
	//
}

ExternalLib::~ExternalLib()
{ }

std::unique_ptr<ExternalLib> ExternalLib::MakeInstance()
{
	return std::make_unique<SoundPlugin>();
}
