
#ifndef AUDIO_BAG_H__
#define AUDIO_BAG_H__

#include "utils/fileStream.h"
#include "audioIndexTypes.h"
#include <vector>
#include <memory>

class AudioBag : public FileStream
{
private:
	std::vector<uint8_t> *m_streamPtr;
	std::shared_ptr<AudioInfo> m_audParam;
	std::vector<uint8_t> m_outputStream;
	bool m_loadState;

public:
	explicit AudioBag();
	virtual ~AudioBag() override;
private:
	virtual void ReportReadComplete(std::vector<uint8_t> *fstream) override;
	virtual void ReportReadFailed() override;
	virtual void ReportWriteReady(std::vector<uint8_t> *fstream) override;
	uint32_t ReadyWriteRawStream();
	void WriteRawStream(uint32_t offset);

	virtual std::vector<uint8_t> GetPrestream();
	virtual std::string OutputFileName();
public:
	void BuildAudioStream(const AudioInfo *audParam);
	void ReleaseFile(const std::string &filename = {});
	bool CheckLoadState() const;
protected:
	std::shared_ptr<AudioInfo> GetAudioParam();
};

#endif