
#ifndef SOUND_PLAYER_H__
#define SOUND_PLAYER_H__

#include "../exportModule.h"
#include <iostream>
#include <Windows.h>
#include <playsoundapi.h>
#include <string>

#pragma comment(lib, "winmm.lib")


enum class SoundFlag : uint32_t
{
	PLAY_ASYNC = SND_ASYNC,
	PLAY_FILENAME = SND_FILENAME,
	PLAY_LOOP = SND_LOOP,
	PLAY_MEMORY = SND_MEMORY,
	PLAY_SYNC = SND_SYNC
};

class SoundPlayer : public ExternalLib
{
private:
	uint32_t m_playFlag;
	std::string m_playSource;

public:
	explicit SoundPlayer(uint32_t flag = static_cast<uint32_t>(SoundFlag::PLAY_FILENAME) | static_cast<uint32_t>(SoundFlag::PLAY_ASYNC));
	virtual ~SoundPlayer();

protected:
	virtual bool PlayBefore();
	virtual void PlayAfter();

public:
	void Play();
	std::string PlaySource() const { return m_playSource; }
};

#endif