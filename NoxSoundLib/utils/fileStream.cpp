
#include "fileStream.h"
#include <iterator>
#include <algorithm>
#include <memory>
#include <functional>

FileStream::FileStream(const std::string &_fileName, FileMode _mode)
	: m_curFileName(_fileName), m_mode(_mode)
{
	m_startReadPos = 0;
	m_maxReadAmount = 0;
}

FileStream::FileStream(const std::string &_fileName, std::vector<uint8_t> &_cpyStream, FileMode _mode)
	: m_curFileName(_fileName), m_mode(_mode)
{
	m_fstream = _cpyStream;
	m_startReadPos = 0;
	m_maxReadAmount = 0;
}

FileStream::FileStream(const FileStream &cpy)
{
	m_curFileName = cpy.m_curFileName;
	m_fstream = cpy.m_fstream;
	m_mode = cpy.m_mode;
	m_startReadPos = cpy.m_startReadPos;
	m_maxReadAmount = cpy.m_maxReadAmount;
}

FileStream::~FileStream()
{ }

void FileStream::ReportReadComplete(std::vector<uint8_t> *stream)
{ }

void FileStream::ReportWriteReady(std::vector<uint8_t> *stream)
{ }

bool FileStream::CheckBeforeRead()
{
	return true;
}

uint32_t FileStream::Read()
{
	uint32_t fSize = 0;
	std::ifstream fin(m_curFileName, std::ios::in | std::ios::binary);
	fin >> std::noskipws;
	if (fin.is_open())
	{
		fin.seekg(0, std::ios::end);
		m_fstream.clear();
		fSize = static_cast<uint32_t>(fin.tellg());
		m_fstream.resize(fSize);
		fin.seekg(0, std::ios::beg);
		std::transform(std::istream_iterator<uint8_t>(fin), std::istream_iterator<uint8_t>(), m_fstream.begin(), [](uint8_t chk) { return chk; });
		fin.close();
		ReportReadComplete(&m_fstream);
	}
	return fSize;
}

uint32_t FileStream::Write()
{
	uint32_t fSize = 0;
	std::ofstream fout(m_curFileName, std::ios::binary);

	if (fout.is_open())
	{
		std::vector<uint8_t> *wrStream = &m_fstream;
		ReportWriteReady(wrStream);

		std::for_each(wrStream->begin(), wrStream->end(), [&fout](uint8_t wrChunk) { fout << wrChunk; });
		fout.close();
		fSize = wrStream->size();
	}
	return fSize;
}

uint32_t FileStream::ReadRange()
{
	std::unique_ptr<std::ifstream, std::function<void(std::ifstream *)>> finPt(new std::ifstream(m_curFileName, std::ios::in | std::ios::binary),
		[](std::ifstream *pfin) { if (pfin->is_open()) pfin->close(); delete pfin; });

	*finPt >> std::noskipws;
	while (finPt->is_open())
	{
		uint32_t maxStream = 0;
		finPt->seekg(0, std::ios::end);
		maxStream = static_cast<uint32_t>(finPt->tellg());
		if (m_startReadPos + m_maxReadAmount >= maxStream)
		{
			ReportReadFailed();
			break;
		}
		finPt->seekg(m_startReadPos, std::ios::beg);
		m_fstream.resize(m_maxReadAmount);
		std::copy_n(std::istream_iterator<uint8_t>(*finPt), m_maxReadAmount, m_fstream.begin());
		ReportReadComplete(&m_fstream);
		return m_maxReadAmount;
	}
	return 0;
}

uint32_t FileStream::FileProcess()
{
	switch (m_mode)
	{
	case FileMode::FILE_MODE_READ_RANGE:
		if (m_maxReadAmount)
			return CheckBeforeRead() ? ReadRange() : 0;
	case FileMode::FILE_MODE_READ:
		return CheckBeforeRead() ? Read() : 0;
	case FileMode::FILE_MODE_WRITE:
		return Write();
	case FileMode::FILE_MODE_OVERWRITE:
		break;
	}
	return 0;
}

uint32_t FileStream::GetStreamLength() const
{
	return m_fstream.size();
}

std::vector<uint8_t> &FileStream::GetFileStream()
{
	return m_fstream;
}

bool FileStream::ChangeFileMode(FileMode _mode)
{
	bool pResult = m_mode != _mode;
	if (pResult)
	{
		m_mode = _mode;
	}
	return pResult;
}

void FileStream::ChangeFileName(const std::string &_fn)
{
	m_curFileName = _fn;
}

void FileStream::ChangeDirectory(const std::string &dirPath)
{
	if (m_curFileName.length())
	{
		uint32_t tokenPos = m_curFileName.find_last_of('\\');

		if (tokenPos == std::string::npos)
			m_curFileName = dirPath + '\\' + m_curFileName;
		else
			m_curFileName = dirPath + m_curFileName.substr(tokenPos, m_curFileName.length() - tokenPos);
	}
}

void FileStream::operator<<(std::vector<uint8_t> &srcStream)
{
	m_fstream.clear();
	m_fstream.resize(srcStream.size());
	std::transform(srcStream.begin(), srcStream.end(), m_fstream.begin(), [](uint8_t uchk)->uint8_t { return uchk; });
}

void FileStream::operator<<(const std::string &srcStrStream)
{
	m_fstream.clear();
	m_fstream.resize(srcStrStream.length());
	std::transform(srcStrStream.begin(), srcStrStream.end(), m_fstream.begin(), [](char chk)->uint8_t { return static_cast<uint8_t>(chk); });
}

void FileStream::SetRange(int offset, int amount)
{
	m_startReadPos = offset;
	m_maxReadAmount = amount;
}

std::string FileStream::CurrentFileName() const
{
	return m_curFileName;
}

void FileStream::SavePreviousMode()
{
	m_previousMode = std::make_tuple(m_curFileName, m_mode);
}

void FileStream::BackPreviousMode()
{
	m_curFileName = std::get<PREVIOUS_FILENAME>(m_previousMode);
	m_mode = std::get<PREVIOUS_FILEMODE>(m_previousMode);
}