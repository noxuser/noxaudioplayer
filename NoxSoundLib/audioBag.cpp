
#include "audioBag.h"
#include <algorithm>

AudioBag::AudioBag()
	: FileStream("audio.bag", FileMode::FILE_MODE_READ_RANGE)
{
	m_streamPtr = nullptr;
	m_loadState = false;
	m_audParam = nullptr;
}

AudioBag::~AudioBag()
{ }

void AudioBag::ReportReadComplete(std::vector<uint8_t> *fstream)
{
	m_streamPtr = fstream;
	m_loadState = true;
}

void AudioBag::ReportReadFailed()
{
	m_loadState = false;
}

void AudioBag::ReportWriteReady(std::vector<uint8_t> *fstream)
{
	*fstream = m_outputStream;
}

uint32_t AudioBag::ReadyWriteRawStream()
{
	std::vector<uint8_t> &wrV = GetPrestream();

	m_outputStream.resize(m_audParam->streamLength + wrV.size());
	std::vector<uint8_t>::iterator wrIt = std::transform(wrV.begin(), wrV.end(), m_outputStream.begin(), [](uint8_t chk) { return chk; });

	return static_cast<uint32_t>(std::distance(m_outputStream.begin(), wrIt));
}

void AudioBag::WriteRawStream(uint32_t offset)
{
	std::copy(m_streamPtr->begin(), m_streamPtr->end(), m_outputStream.begin() + offset);
}

std::vector<uint8_t> AudioBag::GetPrestream()
{
	return{};
}

std::string AudioBag::OutputFileName()
{
	return std::string("play.wav");
}

void AudioBag::BuildAudioStream(const AudioInfo *audParam)
{
	m_audParam = std::make_shared<AudioInfo>(*audParam);

	SetRange(m_audParam->bagOffset_t, m_audParam->streamLength);
	if (FileProcess())
	{
		uint32_t offset = ReadyWriteRawStream();

		WriteRawStream(offset);
	}
}

void AudioBag::ReleaseFile(const std::string &filename)
{
	SavePreviousMode();
	ChangeFileName(filename.length() ? filename : OutputFileName());
	ChangeFileMode(FileMode::FILE_MODE_WRITE);
	m_loadState = FileProcess() != 0;
	BackPreviousMode();
}

bool AudioBag::CheckLoadState() const
{
	return m_loadState;
}

std::shared_ptr<AudioInfo> AudioBag::GetAudioParam()
{
	return m_audParam;
}