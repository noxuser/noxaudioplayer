#define MAKEDLL
#include "soundPlugin.h"
#include "publisher.h"
#include "utils\fileStream.h"
#include "audioIndex.h"
#include "audioKeys.h"
#include "pathManager.h"
#include "wavContext.h"
#include <algorithm>

SoundPlugin::SoundPlugin()
{
	m_state = false;
	m_publish = std::make_unique<Publisher>();
	InitializePlugin();
}

SoundPlugin::~SoundPlugin()
{ }

bool SoundPlugin::CheckState() const
{
	return m_state;
}

/**
* @brief
* ui 로부터 들어온 올바른 경로를 경로 매니저에 전송합니다
*/
bool SoundPlugin::SendPath(const std::string &path)
{
	decltype(m_instance.begin()) instanceIt = m_instance.find(InstanceKey::PATH_MANAGER);

	if (instanceIt != m_instance.end())
	{
		PathManager *pathMan = dynamic_cast<PathManager *>(instanceIt->second.get());

		return pathMan->SetPath(path);
	}
	return false;
}

/**
* @brief
* 경로 다시 받아와서 초기화하는 루틴입니다
* InitInstance 함수 호출로 이어집니다
*/
bool SoundPlugin::Initialize()
{
	//Todo. 뭔가 여기에서 할 것이 있으면..
	return InitInstance();
}

std::list<std::string> SoundPlugin::GetAudioKeyList()
{
	AudioKey *audKey = dynamic_cast<AudioKey *>(m_instance[InstanceKey::AUDIO_KEY].get());

	return audKey->MakeKeyList();
}

std::list<std::string> SoundPlugin::GetAudioRealnameList(const std::string &key)
{
	AudioKey *audKey = dynamic_cast<AudioKey *>(m_instance[InstanceKey::AUDIO_KEY].get());

	return audKey->FindSound(key);
}

void SoundPlugin::PlayFirst(const std::string &key)
{
	AudioKey *audKey = dynamic_cast<AudioKey *>(m_instance[InstanceKey::AUDIO_KEY].get());
	std::string subkey(audKey->FindSoundFirst(key));

	if (subkey.length())
		PlayWithSubkey(subkey);
}

void SoundPlugin::PlayWithSubkey(const std::string &subkey)
{
	m_playkey = subkey;
	Play();
}

bool SoundPlugin::ExtractStream(const std::string &subkey, const std::string &output)
{
	AudioInfo audInfo{ 0, };
	AudioIndex *audIndex = dynamic_cast<AudioIndex *>(m_instance[InstanceKey::AUDIO_INDEX].get());
	if (audIndex->GetAudioData(subkey, &audInfo))
	{
		WavContext *wavCtx = dynamic_cast<WavContext *>(m_instance[InstanceKey::WAV_CONTEXT].get());

		wavCtx->BuildAudioStream(&audInfo);
		wavCtx->ReleaseFile(output);

		return wavCtx->CheckLoadState();
	}
	return false;
}

void SoundPlugin::InitPublisher()
{
	std::string trashkeyword("audio_list.rul");
	std::string pathfile("targetpath.rul");
	
	m_instance.emplace(InstanceKey::AUDIO_INDEX, m_publish->DoPublish<TaskID::idAudioIndex>());
	m_instance.emplace(InstanceKey::PATH_MANAGER, m_publish->DoPublish<TaskID::idPathManager>(pathfile));
	m_instance.emplace(InstanceKey::AUDIO_KEY, m_publish->DoPublish<TaskID::idAudioKey>(trashkeyword));
	m_instance.emplace(InstanceKey::WAV_CONTEXT, m_publish->DoPublish<TaskID::idWavContext>());

	m_instance[InstanceKey::PATH_MANAGER]->FileProcess();
	m_instance[InstanceKey::AUDIO_KEY]->FileProcess();
}

bool SoundPlugin::InitInstance()
{
	PathManager *pathMan = dynamic_cast<PathManager *>(m_instance[InstanceKey::PATH_MANAGER].get());
	std::string path(pathMan->CurrentPath());

	if (path.length())	//인스턴스가 만들어졌으면 여기 아래에서 생성된 인스턴스 초기화를 진행한다
	{
		AudioKey *audKey = dynamic_cast<AudioKey *>(m_instance[InstanceKey::AUDIO_KEY].get());
		
		if (audKey->GetListCount())
		{
			m_instance[InstanceKey::AUDIO_INDEX]->ChangeDirectory(path);
			m_instance[InstanceKey::WAV_CONTEXT]->ChangeDirectory(path);
			return m_instance[InstanceKey::AUDIO_INDEX]->FileProcess() != 0;
		}
	}
	return false;
}

bool SoundPlugin::InitializePlugin()
{
	InitPublisher();
	m_state = InitInstance();
	
	return false;
}

bool SoundPlugin::PlayBefore()
{
	if (!ExtractStream(m_playkey, PlaySource()))
		return false;

	return SoundPlayer::PlayBefore();
}