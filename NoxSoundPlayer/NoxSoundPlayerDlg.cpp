
// NoxSoundPlayerDlg.cpp : 구현 파일
//

#include "stdafx.h"
#include "NoxSoundPlayer.h"
#include "NoxSoundPlayerDlg.h"
#include "afxdialogex.h"

#include <iostream>	//debug

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CNoxSoundPlayerDlg 대화 상자



CNoxSoundPlayerDlg::CNoxSoundPlayerDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_NOXSOUNDPLAYER_DIALOG, pParent), m_bkBrush(RGB(142, 180, 240))
{
	m_bkColor = RGB(142, 180, 240);
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_core = std::make_unique<SoundCore>();
	
	m_core->RegistSignal(SignalTypes::Signal::NOTIFY_INITIALIZE,
		SignalTypes::SignalType(this, [](CDialogEx *cthis) { std::cout << "module load completed!" << std::endl; }));

	m_core->LibModuleInit();

	//!아직 ui 가 초기화 되지 않았으므로 시그널 등록을 립 초기화 이후에 해야한다!//
	m_core->RegistSignal(SignalTypes::Signal::NOTIFY_UI_UPDATE,
		SignalTypes::SignalType(this, [](CDialogEx *cthis) { CNoxSoundPlayerDlg::WhenLoadModuleComplete(cthis); }));
	m_core->RegistSignal(SignalTypes::Signal::NOTIFY_SUBLIST_RELEASE,
		SignalTypes::SignalType(this, [](CDialogEx *cthis) { CNoxSoundPlayerDlg::SendSublistSignal(cthis); }));
	m_core->RegistSignal(SignalTypes::Signal::NOTIFY_FAIL_TO_SEARCH,
		SignalTypes::SignalType(this, [](CDialogEx *cthis) { CNoxSoundPlayerDlg::FailToSearch(cthis); }));
}

void CNoxSoundPlayerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);

	DDX_Control(pDX, PLAYER_PLAY, m_playBtn);
	DDX_Control(pDX, PLAYER_LOADDATA, m_initBtn);
	DDX_Control(pDX, PLAYER_EXTRACT, m_extractBtn);
	DDX_Control(pDX, PLAYER_INPUT, m_inputBtn);
	DDX_Control(pDX, PLAYER_COMBO, m_comboList);
	DDX_Control(pDX, PLAYER_LIST, m_listCtl);
	DDX_Control(pDX, PLAYER_LABEL, m_stateLabel);
	DDX_Control(pDX, PLAYER_SLOT, m_inputMethod);

	InitCControl();
}

void CNoxSoundPlayerDlg::UpdateInitial()
{
	if (m_core->Available())
	{
		m_core->UpdateAudioKeyList();
		m_comboList.UpdateItemList(m_core->GetKeyList());
	}
}

void CNoxSoundPlayerDlg::RequestSublist(const CString &cKey)
{
	m_core->StoreSubListFromKey(cKey);
}

void CNoxSoundPlayerDlg::SendSublist()
{
	//Todo. 여기서 코어에 캐칭된 서브리스트 읽어서 리스트ui 로 보낸다
	m_listCtl.UpdateListItem(m_core->GetSubList());
}

void CNoxSoundPlayerDlg::WhenLoadModuleComplete(CDialogEx *cThis)		//시그널 수신
{
	CNoxSoundPlayerDlg *cWnd = dynamic_cast<CNoxSoundPlayerDlg *>(cThis);

	if (cWnd != nullptr)
	{
		cWnd->UpdateInitial();
	}
}

void CNoxSoundPlayerDlg::RequestSublistSignal(CDialogEx *cThis, const CString &cKey)
{
	CNoxSoundPlayerDlg *cWnd = dynamic_cast<CNoxSoundPlayerDlg *>(cThis);

	if (cWnd != nullptr)
	{
		//여기서 서브리스트 받고
		//리스트 ui에 전송해준다
		cWnd->RequestSublist(cKey);
	}
}

void CNoxSoundPlayerDlg::SendSublistSignal(CDialogEx *cThis)
{
	CNoxSoundPlayerDlg *cWnd = dynamic_cast<CNoxSoundPlayerDlg *>(cThis);

	if (cWnd != nullptr)
	{
		cWnd->SendSublist();
	}
}

void CNoxSoundPlayerDlg::FailToSearch(CDialogEx *cThis)
{
	CNoxSoundPlayerDlg *cWnd = dynamic_cast<CNoxSoundPlayerDlg *>(cThis);

	if (cWnd != nullptr)
	{
		::AfxMessageBox(L"입력하신 항목을 찾을 수 없습니다");
	}
}

void CNoxSoundPlayerDlg::InitCControl()
{
	CString captions;

	captions.LoadStringW(IDS_PLAYSOUND);
	m_playBtn.ChangeButtonCaption(captions);
	captions.LoadStringW(IDS_FINDNOX_PATH);
	m_initBtn.ChangeButtonCaption(captions);
	captions.LoadStringW(IDS_SOUND_INPUT);
	m_inputBtn.ChangeButtonCaption(captions);
	captions.LoadStringW(IDS_SOUND_EXTRACT);
	m_extractBtn.ChangeButtonCaption(captions);
	UpdateLoadDataState();

	
	m_listCtl.ListInitialize();
	m_core->ForceEmitUIUpdateSignal();	//리스트 업뎃 요청
	using ListCbType = CDropListBox::ListCbType;
	
	m_comboList.SetCallback(std::make_unique<ListCbType>(this, [](CDialogEx *cThis, const CString &cKey) { CNoxSoundPlayerDlg::RequestSublistSignal(cThis, cKey); }));
}

void CNoxSoundPlayerDlg::UpdateLoadDataState()
{
	CString caption;

	caption.LoadStringW(m_core->Available() ? IDS_STATE_ENABLE : IDS_STATE_DISABLE);
	m_stateLabel.SetWindowTextW(caption);
}

BEGIN_MESSAGE_MAP(CNoxSoundPlayerDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(PLAYER_PLAY, ClickPlaySound)
	ON_BN_CLICKED(PLAYER_LOADDATA, ClickLoadData)
	ON_BN_CLICKED(PLAYER_EXTRACT, ClickExtractSound)
	ON_BN_CLICKED(PLAYER_INPUT, ClickInputSound)
	ON_WM_CTLCOLOR()
END_MESSAGE_MAP()


// CNoxSoundPlayerDlg 메시지 처리기

BOOL CNoxSoundPlayerDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 시스템 메뉴에 "정보..." 메뉴 항목을 추가합니다.

	// IDM_ABOUTBOX는 시스템 명령 범위에 있어야 합니다.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 이 대화 상자의 아이콘을 설정합니다.  응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.

	// TODO: 여기에 추가 초기화 작업을 추가합니다.

	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

void CNoxSoundPlayerDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다.  문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CNoxSoundPlayerDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트입니다.

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 아이콘을 그립니다.
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CNoxSoundPlayerDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CNoxSoundPlayerDlg::ClickPlaySound()
{
	CString item(m_listCtl.GetSelectItemString());

	if (item.GetLength())
	{
		m_core->Play(item);
		//사운드 재생 버튼입니다
	}
}

void CNoxSoundPlayerDlg::ClickExtractSound()
{
	CString item(m_listCtl.GetSelectItemString());
	//사운드 추출 버튼입니다
	if (item.GetLength())
	{
		m_core->Extract(item);
	}
}

void CNoxSoundPlayerDlg::ClickLoadData()
{
	if (m_core->Available())
	{
		CString message;

		message.LoadStringW(IDS_ALREADY_LOADING);
		::AfxMessageBox(message);
	}
	else
	{
		CString szFilter(L"Nox exe file (*.exe)|*.exe|All Files (*.*)|*.*||");
		CFileDialog fileDlg(TRUE, L"FileDialog", nullptr, OFN_HIDEREADONLY, szFilter.GetString(), this);
		if (fileDlg.DoModal() == IDOK)
		{
			if (m_core->ResetPath(fileDlg.GetPathName()))
			{
				//Todo. 선택된 타겟이 유효하다면 여기에서 초기화 작업을 합니다
				//이때, 이미 초기화 된 객체는 작업에서 건너뜁니다
				//만약 위 같이 하였을 때 정 설계가 안되면, 다시 초기화 해도 지장이 없을경우에 한에서만 초기화를 또 해줍니다
				m_core->LibModuleInit();
			}
			else
			{
				//Todo. 유효하지 않은 타겟입니다
				::AfxMessageBox(L"Do not nox file!");
			}
		}
	}
}

void CNoxSoundPlayerDlg::ClickInputSound()
{
	CString input;

	m_inputMethod.GetWindowTextW(input);
	if (input.GetLength())
	{
		m_core->PlayFromInput(input);
		m_inputMethod.SetSel(0, -1);
		m_inputMethod.Clear();
	}
}

HBRUSH CNoxSoundPlayerDlg::OnCtlColor(CDC *pDC, CWnd *pWnd, UINT nCtlColor)
{
	CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

	pDC->SetBkColor(m_bkColor);
	return m_bkBrush;
}

BOOL CNoxSoundPlayerDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.
	if (pMsg->message == WM_KEYDOWN)
	{
		if (pMsg->wParam == VK_RETURN) // ENTER키 눌릴 시
			return TRUE;
		if (pMsg->wParam == VK_ESCAPE) // ESC키 눌릴 시
			return TRUE;
	}

	return CDialogEx::PreTranslateMessage(pMsg);
}
