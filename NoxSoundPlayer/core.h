
#ifndef SOUND_LIB_CORE_H__
#define SOUND_LIB_CORE_H__

#include "signalTypes.h"
#include <memory>
#include <iostream>
#include <list>
#include <map>


class ExternalLib;

class SoundCore
{

private:
	bool m_initComplete;
	std::unique_ptr<ExternalLib> m_libModule;
	std::list<std::string> m_keylist;
	std::map<std::string, std::string> m_keylistCpy;
	std::map<SignalTypes::Signal, SignalTypes::SignalType> m_signalMap;
	std::list<std::string> m_subAudioList;

public:
	explicit SoundCore();
	virtual ~SoundCore();

	bool Available() const;
	void LibModuleInit();
	bool ResetPath(const CString &cPath);
	void Play(const CString &cSubkey);
	void PlayFromInput(const CString &input);
	bool Extract(const CString &subkey);
	void ForceEmitUIUpdateSignal(const CString &initKey = {});
	uint32_t UpdateAudioKeyList();
	std::list<std::string> GetKeyList() const;
	void StoreSubListFromKey(const CString &cKey);
	std::list<std::string> GetSubList() const;

	//시그널 등록
	void RegistSignal(SignalTypes::Signal signalNumber, SignalTypes::SignalType &&signalData);
	//방출은 함수 내에서
	void EmitSignal(SignalTypes::Signal signalNumber);
};

#endif