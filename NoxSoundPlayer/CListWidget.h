
#ifndef CLIST_WIDGET_H__
#define CLIST_WIDGET_H__

#include <list>
#include <string>

class CListWidget : public CListCtrl
{
public:
	explicit CListWidget();
	void UpdateListItem(const std::list<std::string> &itemlist);
	CString GetSelectItemString();
	void ListInitialize();
};

#endif