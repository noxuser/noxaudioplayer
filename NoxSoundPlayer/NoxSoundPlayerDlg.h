
// NoxSoundPlayerDlg.h : 헤더 파일
//

#pragma once

#include "CColorBtn.h"
#include "CDropListBox.h"
#include "CListWidget.h"
#include <memory>
#include "core.h"


// CNoxSoundPlayerDlg 대화 상자
class CNoxSoundPlayerDlg : public CDialogEx
{
// 생성입니다.
public:
	CNoxSoundPlayerDlg(CWnd* pParent = NULL);	// 표준 생성자입니다.

// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_NOXSOUNDPLAYER_DIALOG };
#endif
private:
	std::unique_ptr<SoundCore> m_core;
	CColorBtn m_playBtn;
	CColorBtn m_initBtn;
	CColorBtn m_extractBtn;
	CColorBtn m_inputBtn;
	CDropListBox m_comboList;
	CListWidget m_listCtl;
	CStatic m_stateLabel;
	CEdit m_inputMethod;

private:
	//@Colors
	UINT m_bkColor;
	CBrush m_bkBrush;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.

private:
	void UpdateInitial();
	void RequestSublist(const CString &cKey);
	void SendSublist();

public:
	//!@Signals
	static void WhenLoadModuleComplete(CDialogEx *cThis);	//시그널
	static void RequestSublistSignal(CDialogEx *cThis, const CString &cKey);	//드롭다운 ui 로 부터 나오는 시그널
	static void SendSublistSignal(CDialogEx *cThis);	//리스트 ui 로 보낼 준비완료
	static void FailToSearch(CDialogEx *cThis);

// 구현입니다.
private:
	void InitCControl();
	void UpdateLoadDataState();

protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void ClickPlaySound();
	afx_msg void ClickExtractSound();
	afx_msg void ClickLoadData();
	afx_msg void ClickInputSound();
	afx_msg HBRUSH CNoxSoundPlayerDlg::OnCtlColor(CDC *pDC, CWnd *pWnd, UINT nCtlColor);
	afx_msg BOOL PreTranslateMessage(MSG* pMsg);
	DECLARE_MESSAGE_MAP()
};
