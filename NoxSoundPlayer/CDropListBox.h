
#ifndef CDROP_LIST_BOX_H__
#define CDROP_LIST_BOX_H__

#include <list>
#include <string>
#include <tuple>
#include <functional>
#include <memory>

class CDropListBox : public CComboBox
{
public:
	using ListCbType = std::tuple<CDialogEx*, std::function<void(CDialogEx*, const CString&)>>;

private:
	CBrush m_bkBrush;
	UINT m_bkColor;
	std::unique_ptr<ListCbType> m_cbElement;

	enum
	{
		LISTENER,
		SIGNAL_METHOD
	};

public:
	explicit CDropListBox();
	virtual ~CDropListBox() override;
	
public:
	void ChangeBackgroundColor(UINT color);
	void UpdateItemList(const std::list<std::string> &itemList);

	void SetCallback(std::unique_ptr<ListCbType> &&cbDataPtr);

protected:
	afx_msg HBRUSH OnCtlColor(CDC *pDC, CWnd *pWnd, UINT nCtlColor);
	afx_msg BOOL OnCommand(WPARAM wParam, LPARAM lParam);
	afx_msg void OnSelectedItemChanged();
	DECLARE_MESSAGE_MAP()
};

#endif