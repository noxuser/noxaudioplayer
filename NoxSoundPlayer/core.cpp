
#include "stdafx.h"
#include "core.h"
//#include "soundPlugin.h"
#include "exportModule.h"
#include "utils\stringUtils.h"

#include <utility>
#include <algorithm>

SoundCore::SoundCore()
{
	m_libModule = nullptr;
	m_initComplete = false;
}

SoundCore::~SoundCore() = default;

bool SoundCore::Available() const
{
	return m_initComplete;
}

/**
*@brief
* 코어가 인스턴스 되었을 때에만 호출됩니다
*/
void SoundCore::LibModuleInit()
{
	if (m_libModule == nullptr)
	{
		//m_libModule = std::make_unique<SoundPlugin>();
		m_libModule = ExternalLib::MakeInstance();
		m_initComplete = m_libModule->CheckState();
		EmitSignal(SignalTypes::Signal::NOTIFY_UI_UPDATE);
	}
	else if (!m_initComplete)
	{
		m_initComplete = m_libModule->Initialize();
		EmitSignal(SignalTypes::Signal::NOTIFY_UI_UPDATE);
	}
}

/**
* @brief
* 초기화에 실패한 경우 ui 로 부터 올바른 경로를 얻어와 다시 초기화를 시도합니다
*/
bool SoundCore::ResetPath(const CString &cPath)
{
	std::string destPath;
	std::wstring srcPath(cPath.operator LPCWSTR());
	StringUtils::UnicodeToAnsi(srcPath, destPath);
	return m_libModule->SendPath(destPath);
}

void SoundCore::Play(const CString &cSubkey)
{
	//Todo. 오디오 재생
	if (cSubkey.GetLength())
	{
		std::string subkey;
		std::wstring tmp(cSubkey.operator LPCWSTR());
		StringUtils::UnicodeToAnsi(tmp, subkey);
		if (subkey.length())
			m_libModule->PlayWithSubkey(subkey);
	}
}

void SoundCore::PlayFromInput(const CString &input)
{
	std::string userkey;
	std::wstring srcPath(input.operator LPCWSTR());

	StringUtils::UnicodeToAnsi(srcPath, userkey);
	std::transform(userkey.cbegin(), userkey.cend(), userkey.begin(), ::tolower);
	decltype(m_keylistCpy.begin()) keyfind = m_keylistCpy.find(userkey);
	if (keyfind != m_keylistCpy.end())
	{
		m_libModule->PlayFirst(keyfind->second);
	}
	else
		EmitSignal(SignalTypes::Signal::NOTIFY_FAIL_TO_SEARCH);
}

bool SoundCore::Extract(const CString &subkey)
{
	//Todo. 오디오 추출
	std::string subkeyDest;
	std::wstring srcPath(subkey.operator LPCWSTR());
	StringUtils::UnicodeToAnsi(srcPath, subkeyDest);
	m_libModule->ExtractStream(subkeyDest);

	return false;
}


void SoundCore::ForceEmitUIUpdateSignal(const CString &initKey)
{
	EmitSignal(SignalTypes::Signal::NOTIFY_UI_UPDATE);

	StoreSubListFromKey(initKey.GetLength() ? initKey : (m_keylist.size() ? CString(m_keylist.front().c_str()) : CString())); //27 feb 2021 23:10 예외처리 추가
}

uint32_t SoundCore::UpdateAudioKeyList()
{
	std::map<std::string, std::string> &keyMirrorM = m_keylistCpy;
	m_keylist = m_libModule->GetAudioKeyList();

	std::for_each(m_keylist.begin(), m_keylist.end(), [&keyMirrorM](std::string &item)
	{
		std::string lowkey(item.size(), 0);

		std::transform(item.cbegin(), item.cend(), lowkey.begin(), ::tolower);
		keyMirrorM.emplace(lowkey, item);
	});
	return m_keylist.size();
}

std::list<std::string> SoundCore::GetKeyList() const
{
	return m_keylist;
}

void SoundCore::StoreSubListFromKey(const CString &cKey)
{
	if (cKey.GetLength())
	{
		std::string findKey;
		std::wstring srcPath(cKey.operator LPCWSTR());

		StringUtils::UnicodeToAnsi(srcPath, findKey);
		m_subAudioList = m_libModule->GetAudioRealnameList(findKey);

		EmitSignal(SignalTypes::Signal::NOTIFY_SUBLIST_RELEASE);
	}
}

std::list<std::string> SoundCore::GetSubList() const
{
	return m_subAudioList;
}

void SoundCore::RegistSignal(SignalTypes::Signal signalNumber, SignalTypes::SignalType &&signalData)
{
	if (m_signalMap.find(signalNumber) == m_signalMap.end())
	{
		m_signalMap.emplace(signalNumber, std::forward<SignalTypes::SignalType>(signalData));
	}
}

void SoundCore::EmitSignal(SignalTypes::Signal signalNumber)
{
	decltype(m_signalMap.begin()) findIterator = m_signalMap.find(signalNumber);

	if (findIterator != m_signalMap.end())
	{
		SignalTypes::SignalType signal = findIterator->second;

		(std::get<SignalTypes::SIGNAL_DATA_SIGNAL>(signal))(std::get<SignalTypes::SIGNAL_DATA_CTHIS>(signal));
	}
}